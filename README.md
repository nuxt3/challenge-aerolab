<p align="center">
<a href="https://nuxtjs.org" target="_blank"><img src="https://warlord0blog.files.wordpress.com/2019/10/nuxtjs_logo.png" width="200"></a>
<a href="https://vuejs.org/" target="_blank"><img src="https://www.arsys.es/blog/file/uploads/2020/04/01-vuejs.jpg" width="200"></a>
<a href="https://vuetifyjs.com/en" target="_blank"><img src="https://squmarigames.com/wp-content/uploads/2020/09/Vuetify.png" width="200"></a>
<a href="https://id.heroku.com/login" target="_blank"><img src="https://estebanromero.com/wp-content/uploads/2018/02/heroku1.png" width="200"></a>
</p>

## Aerolab Challenge

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev


```
## Tech's
For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org). <br/>
For detailed explanation on how things work UI framework, check out [Vue.js docs](https://vuejs.org/). <br/>
For detailed explanation on how things work JS framework, check out [Vuetify.js docs](https://vuetifyjs.com/en/).<br/>
Documentation for deploy Nuxt app on Heroku (https://medium.com/@brian0592/deploy-app-nuxt-js-en-heroku-1a35fdb099d3)<br/>
