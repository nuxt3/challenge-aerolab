export default {
methods: {
        alert(status) {
            this.$swal.fire({
                type: status.type,
                title: status.message,
                footer: '<a href="/redeemHistory">My Redeems</a>',
            });
     }
},
}