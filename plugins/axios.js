export default function ({ $axios, redirect }) {

  $axios.setBaseURL('https://coding-challenge-api.aerolab.co')
  $axios.setToken('eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MGQzOTIxNzY3Mjk2ZTAwMTk5NjQxMzMiLCJpYXQiOjE2MjQ0NzgyMzF9.hrH2DaH4aaohvV01N8WvZ7aBhU1708gOkyDxpj9qOJU', 'Bearer')

  $axios.onRequest(config => {
      console.log('Making request to ' + config.url)
    })
  
  $axios.onError(error => {
      const code = parseInt(error.response && error.response.status)
      if (code === 400) {
        redirect('/400')
      }
    })
  }