export const state = () => ({});
export const getters = {};
// Actions
export const actions = {
 async nuxtServerInit({ dispatch }) {
        await dispatch('user/fetchUserData');
        await dispatch('products/fetchProductsData');
    }
};