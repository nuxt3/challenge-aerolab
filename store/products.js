export const state = () => ({
    products: [],
    statusTransaction: {
        message: "",
        type: ""
    }
});

export const mutations = {
    setProductData(state, payload) {
        state.products = payload;
    },
    setStatusTransaction(state, payload) {
        state.statusTransaction = {
            message: payload.message,
            type: payload.type
        }
    },
    filterProducts(state, sortBy) {
        if (sortBy === 'priceLow') {
            state.products.sort((a, b) => { return a.cost - b.cost });
        }
        if (sortBy === 'priceHigh') {
            state.products.sort((a, b) => { return b.cost - a.cost });
        }
    }
};

export const actions = {
    async fetchProductsData({ commit }) {
        const data = await this.$axios.$get('/products');
        commit('setProductData', data);
    },
    async redeemProduct({ context, commit }, productID) {
        const data = await this.$axios.$post('/redeem', { productId: productID })
            .then(response => {
                let status = { message: "Success Redeem!", type: 'success' };
                commit('setStatusTransaction', status)
            }).catch(error => {
                let status = { message: "Something went wrong", type: 'warning' };
                commit('setStatusTransaction', status);
            });
    },
};
export const getters = {
    getProducts: state => state.products,
    getStatusTransaction: state => state.statusTransaction
}