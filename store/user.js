export const state = () => ({
    userData: {},
    redeemHistory: []
});
  
export const mutations = {
    setUserData(state, payload) {
        state.userData = payload;
    },
    setRedeemHistory(state, payload) {
        state.redeemHistory = payload;
    }
};

export const actions = {
    async fetchUserData( { commit } ) {
        const data  = await this.$axios.$get('/user/me');
            commit('setUserData', data);
    },
    async fetchRedeemHistory({ commit }) {
        const data = await this.$axios.$get('/user/history');
        commit('setRedeemHistory', data);
    },
};
export const getters = {
    getUser : state => ( state.userData ),
    getRedeemHistory : state => ( state.redeemHistory ),
}
